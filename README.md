# A basic arch installation guide for hardware machine with the drive /dev/nvme0n1
The arch iso can be downloaded here: https://archlinux.org/download/ or directly [here](https://mirror.alwyzon.net/archlinux/iso/2023.08.01/archlinux-2023.08.01-x86_64.iso "austrian mirror")
## wipe nvme0n1
```
wipefs -a /dev/nvme0n1
wipefs -a /dev/nvme1n1
wipefs -a /dev/sda
wipefs -a /dev/sdc

```

## create partitions
```
echo -e "n\np\n1\n\n+256M\nn\np\n2\n\n+30G\nn\np\n\n\n\np\nw\n" | fdisk /dev/nvme0n1 
```

## format partitions and label them
```
yes | mkfs.fat /dev/nvme0n1p1 && fatlabel /dev/nvme0n1p1 BOOT && 
yes | mkfs.ext4 -L ROOT /dev/nvme0n1p2 && 
yes | mkfs.ext4 -L HOME /dev/nvme0n1p3 && 
yes | mkfs.ext4 -L Storage_01 /dev/sda && 
yes | mkfs.ext4 -L Storage_02 /dev/sdc && 
yes | mkfs.ext4 -L Storage_NVME /dev/nvme1n1
```

## mount partitions
```
mount /dev/nvme0n1p2 /mnt && 
mkdir /mnt/{boot,home,Storage_NVME,Storage_01,Storage_02} && 
mount /dev/nvme0n1p1 /mnt/boot && 
mount /dev/nvme0n1p3 /mnt/home && 
mount /dev/nvme1n1 /mnt/Storage_NVME && 
mount /dev/sda /mnt/Storage_01 && 
mount /dev/sdc /mnt/Storage_02
```

## set ntp
```
timedatectl set-ntp true
```

## change mirrors
```
sudo reflector --latest 20 --protocol https --sort rate --save /etc/pacman.d/mirrorlist
```

## enable parallel downloads
```
sed -i '/#ParallelDownloads/s/^#//g' /etc/pacman.conf
```

## speed up compiling
```
sed -i '/#MAKEFLAGS/s/^#//g' /etc/makepkg.conf && sed -i 's/-j2/-j$(nproc)/' /etc/makepkg.conf
```


## install basic system and packages
```
pacstrap /mnt base base-devel linux linux-firmware python3 grub efibootmgr os-prober networkmanager ntp openssh vim neofetch ansible
```

## generate fstab
```
genfstab -U /mnt >> /mnt/etc/fstab
```

## chroot into new system
```
arch-chroot /mnt
```

## enable services
```
systemctl enable NetworkManager.service && systemctl enable sshd.service
```

## setup locale etc.
```
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen && echo "KEYMAP=us" >> /etc/vconsole.conf && echo "LANG=en_US.UTF-8" >> /etc/locale.conf && locale-gen && echo "LANG=en_US.UTF-8" >> /etc/locale.conf
```

## setup grub
```
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub && grub-mkconfig -o /boot/grub/grub.cfg
```

## setup root password
```
passwd
```

## add user stywen
```
useradd -m stywen && usermod -G wheel stywen && passwd stywen
```

## enable wheel
```
sed -i "s/# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/g" /etc/sudoers
```

## setup multilib
```
sed -i "/\[multilib\]/,/Include/"'s/^#//' /etc/pacman.conf && sudo pacman -Syyuu
```
